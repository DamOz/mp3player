module mp3player {
        requires javafx.graphics;
        requires javafx.controls;
        requires javafx.fxml;
        requires jid3lib;
        requires javafx.media;

        exports pl.damianozga.mp3player to javafx.graphics;
        opens pl.damianozga.controller to javafx.fxml;
        opens pl.damianozga.mp3 to javafx.base;
}